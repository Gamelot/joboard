<?php

namespace App\Controller;

use App\Entity\Job;
use App\Form\JobType;
use App\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/job")
 */
class JobController extends AbstractController
{
    /**
     * @Route("/", name="job_index", methods={"GET"})
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository(Job::class)->findAll();

        //$query = $em->createQuery(
        //    'SELECT j FROM App\Entity\Job j WHERE j.expires_at > :date'
        //    )->setParameter('date', date('Y-m-d H:i:s', time() - 86400 * 30));

        //$entities = $em->getRepository('App:Job')->getActiveJobs();

        $categories = $em->getRepository('App:Category')->getWithJobs();

        foreach($categories as $category) {
            $category->setActiveJobs($em->getRepository('App:Job')->getActiveJobs($category->getId(), $this->getParameter('max_jobs_on_homepage')));
        }

        $activeJobsCount = $em->getRepository('App:Job')->countActiveJobs($category->getId());

        if ($activeJobsCount >= $this->getParameter('max_jobs_on_homepage')) {
            $activeJobsCount -= $this->getParameter('max_jobs_on_homepage');
            $category->setMoreJobs($activeJobsCount);
        }
        
        //$entities = $query->getResult();

        // return $this->render('job/index.html.twig', array(
        //     'entities' => $entities,
        // ));
        return $this->render('job/index.html.twig', array(
            'categories' => $categories
        ));
    }

    /**
     * @Route("/new", name="job_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $job = new Job();
        $form = $this->createForm(JobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job);
            $entityManager->flush();

            return $this->redirectToRoute('job_index');
        }

        return $this->render('job/new.html.twig', [
            'job' => $job,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="job_show", methods={"GET"})
     */
    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();

        //$entity = $em->getRepository(Job::class)->find($id);
        $entity = $em->getRepository(Job::class)->getActiveJob($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        return $this->render('job/show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * @Route("/{id}/edit", name="job_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Job $job): Response
    {
        $form = $this->createForm(JobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('job_index');
        }

        return $this->render('job/edit.html.twig', [
            'job' => $job,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="job_delete", methods={"POST"})
     */
    public function delete(Request $request, Job $job): Response
    {
        if ($this->isCsrfTokenValid('delete'.$job->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($job);
            $entityManager->flush();
        }

        return $this->redirectToRoute('job_index');
    }
}
