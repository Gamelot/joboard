<?php

# src/App/Controller/CategoryController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Response;

/**
 * Category controller
 */
class CategoryController extends AbstractController
{
    public function show($slug, $page): Response
    {
        // $em = $this->getDoctrine()->getManager();

        // $category = $em->getRepository('App:Category')->findOneBySlug($slug);

        // if (!$category) {
        //     throw $this->createNotFoundException('Такая категория не найдена.');
        // }

        // $category->setActiveJobs($em->getRepository('App:Job')->getActiveJobs($category->getId()));

        // return $this->render('category/show.html.twig', array(
        //     'category' => $category,
        // ));
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('App:Category')->findOneBySlug($slug);

        if (!$category) {
            throw $this->createNotFoundException('Такая категория не найдена.');
        }

        $totalJobs    = $em->getRepository('App:Job')->countActiveJobs($category->getId());
        $jobsPerPage  = $this->getParameter('max_jobs_on_category');
        $lastPage     = ceil($totalJobs / $jobsPerPage);
        $previousPage = $page > 1 ? $page - 1 : 1;
        $nextPage     = $page < $lastPage ? $page + 1 : $lastPage;
        $activeJobs   = $em->getRepository('App:Job')
            ->getActiveJobs($category->getId(), $jobsPerPage, ($page - 1) * $jobsPerPage);

        $category->setActiveJobs($activeJobs);

        return $this->render('category/show.html.twig', array(
            'category'     => $category,
            'lastPage'     => $lastPage,
            'previousPage' => $previousPage,
            'currentPage'  => $page,
            'nextPage'     => $nextPage,
            'totalJobs'    => $totalJobs
        ));
    }
}
